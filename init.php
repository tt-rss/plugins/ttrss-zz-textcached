<?php
class Zz_Text_Cached extends Plugin {
	private $host;

	function about() {
		return array(1.0,
			"Rewrite destinations of text links if local cache exists",
			"fox");
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_RENDER_ARTICLE, $this);
		$host->add_hook($host::HOOK_RENDER_ARTICLE_CDM, $this);
	}

	function hook_render_article($article) {
		return $this->hook_render_article_cdm($article);
	}

	function hook_render_article_cdm($article) {

		$doc = new DOMDocument();

		$found = false;

		if (@$doc->loadHTML($article["content"])) {

			$xpath = new DOMXpath($doc);
			$elems = $xpath->query('(//*[@href])');

			foreach ($elems as $elem) {
				$url = $elem->getAttribute("href");
				$local_filename = CACHE_DIR . "/images/" . sha1($url);

				if (file_exists($local_filename) && filesize($local_filename) > 0) {
					$local_url = get_self_url_prefix() . '/public.php?op=cached_url&hash=' . sha1($url);

					$elem->setAttribute("href", $local_url);
	
					/*$elem->nodeValue = "";
					$elem->appendChild($doc->createTextNode($local_url));*/

					$found = true;
				}
			}
		}

		if ($found) {
			$article["content"] = $doc->saveXML();
		}

		return $article;
	}


	function api_version() {
		return 2;
	}

}
?>
